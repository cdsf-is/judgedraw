use anyhow::Result;
use itertools::Itertools;

use crate::{io::reporter::Reporter, model::judge::Judge};

pub fn format<'j, I>(judges: I) -> String
where
    I: Iterator<Item = &'j Judge>,
{
    judges.map(|judge| &judge.label).sorted().join(", ")
}

pub fn dump<'j, I>(reporter: &mut Reporter, label: &str, judges: I) -> Result<()>
where
    I: Iterator<Item = &'j Judge>,
{
    reporter.labeledln(label, format(judges))
}
