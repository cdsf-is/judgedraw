use std::collections::{HashMap, HashSet};

use super::{
    category_drawer,
    drawer::{CategoryToDraw, EventContext},
    equalizer::Equalizer,
    prng::Generator,
};
use crate::{
    io::reporter::Reporter,
    model::{
        buckets::Buckets, buckettype::BucketType, category::Category,
        drawingspecification::DrawingSpecification, eventdraw::EventDraw,
    },
    Parameters,
};
use anyhow::Result;

pub fn draw<'j, 'o>(
    parameters: Parameters,
    reporter: &'o mut Reporter,
    generator: &'o mut Generator,
    drawing_specification: &'o DrawingSpecification,
    buckets: &'j Buckets<'j>,
    verbose: bool,
) -> Result<EventDraw<'j>> {
    let mut context = EventContext {
        reporter,
        generator,
        equalizer: HashMap::new(),
        limit: parameters.limit,
        to_draw: HashSet::new(),
        drawn: HashSet::new(),
        verbose,
    };

    let equalize = parameters.equalize;

    let categories_to_draw =
        select_categories_to_draw(parameters, &mut context, drawing_specification)?;

    context.equalizer.insert(
        BucketType::Domestic,
        Equalizer::new(&buckets.get(&BucketType::Domestic).get_judges(), equalize),
    );
    context.equalizer.insert(
        BucketType::Foreign,
        Equalizer::new(&buckets.get(&BucketType::Foreign).get_judges(), equalize),
    );
    context.to_draw = categories_to_draw
        .iter()
        .map(|category_to_draw| category_to_draw.category.clone())
        .collect();

    execute_draw(&mut context, categories_to_draw, buckets)
}

fn select_categories_to_draw(
    parameters: Parameters,
    context: &mut EventContext,
    drawing_specification: &DrawingSpecification,
) -> Result<Vec<CategoryToDraw>> {
    let Parameters {
        judging_type,
        mut categories,
        foreign,
        randomize_categories_order,
        ..
    } = parameters;

    let randomized_categories = if randomize_categories_order {
        (0..categories.len())
            .rev()
            .map(|remaining| {
                let seeded = context.generator.get_random_usize(remaining);
                categories.swap_remove(seeded)
            })
            .collect()
    } else {
        categories
    };

    randomized_categories
        .iter()
        .map(|category: &Category| {
            let category = category.clone();
            let drawing_counts =
                drawing_specification.get_drawing_counts(&category, &judging_type, foreign)?;
            Ok(CategoryToDraw {
                category,
                drawing_counts,
            })
        })
        .collect()
}

fn execute_draw<'j>(
    context: &mut EventContext,
    categories_to_draw: Vec<CategoryToDraw>,
    buckets: &'j Buckets<'j>,
) -> Result<EventDraw<'j>> {
    let mut event_draw = EventDraw::new();
    for category_to_draw in categories_to_draw {
        let category_draw = category_drawer::draw(context, &category_to_draw, buckets)?;
        event_draw.set_category(category_to_draw.category.clone(), category_draw);

        context.to_draw.remove(&category_to_draw.category);
        context.drawn.insert(category_to_draw.category);
    }

    if context.verbose {
        dump_counts(context)?;
    }

    Ok(event_draw)
}

fn dump_counts(context: &mut EventContext) -> Result<()> {
    context.reporter.ln("---")?;
    context.reporter.labeledln(
        "  domestic judges counts: ",
        context.get_equalizer(BucketType::Domestic).to_string(),
    )?;
    context.reporter.labeledln(
        "  foreigners counts: ",
        context.get_equalizer(BucketType::Foreign).to_string(),
    )?;

    Ok(())
}
