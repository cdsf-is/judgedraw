use std::collections::{HashMap, HashSet};

use super::{equalizer::Equalizer, prng::Generator};
use crate::{
    io::reporter::Reporter,
    model::{
        buckettype::BucketType, drawingspecification::CategoryDrawingCounts,
        category::Category,
    },
};

pub(crate) struct CategoryToDraw {
    pub category: Category,
    pub drawing_counts: CategoryDrawingCounts,
}

pub(crate) struct EventContext<'o> {
    pub reporter: &'o mut Reporter,
    pub generator: &'o mut Generator,
    pub equalizer: HashMap<BucketType, Equalizer>,
    pub limit: u64,
    pub to_draw: HashSet<Category>,
    pub drawn: HashSet<Category>,
    pub verbose: bool,
}

impl<'o> EventContext<'o> {
    pub fn get_equalizer(&self, mut bucket_type: BucketType) -> &Equalizer {
        if bucket_type != BucketType::Foreign {
            bucket_type = BucketType::Domestic;
        }
        self.equalizer.get(&bucket_type).unwrap()
    }

    pub fn get_equalizer_mut(&mut self, mut bucket_type: BucketType) -> &mut Equalizer {
        if bucket_type != BucketType::Foreign {
            bucket_type = BucketType::Domestic;
        }
        self.equalizer.get_mut(&bucket_type).unwrap()
    }
}
