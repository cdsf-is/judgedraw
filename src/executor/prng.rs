use rand_mt::Mt64 as MersenneTwister;
use sha2::{Digest, Sha256};
use std::convert::TryFrom;

type Seed = u64;

pub struct Generator {
    twister: MersenneTwister,
    seed: Seed,
    new_seed: bool,
}

impl Generator {
    pub fn init_as_random() -> Generator {
        let seed = Generator::create_new_seed();

        Generator {
            twister: MersenneTwister::new(seed),
            seed,
            new_seed: true,
        }
    }

    pub fn init_from_seed(seed_string: &str) -> Generator {
        let seed_vector = hex::decode(seed_string).unwrap();
        let seed_bytes = seed_vector.as_slice();
        let seed_array = <[u8; 8]>::try_from(seed_bytes).unwrap();

        let seed = u64::from_le_bytes(seed_array);

        Generator {
            twister: MersenneTwister::new(seed),
            seed,
            new_seed: false,
        }
    }

    pub fn get_seed_is_new(&self) -> bool {
        self.new_seed
    }

    pub fn get_seed(&self) -> String {
        hex::encode(self.seed.to_le_bytes())
    }

    pub fn get_random(&mut self) -> f64 {
        self.twister.next_u64() as f64 / u64::MAX as f64
    }

    pub fn get_random_usize(&mut self, upper_bound: usize) -> usize {
        let scaled = self.get_random() * (upper_bound as f64);
        scaled.trunc() as usize
    }

    fn create_new_seed() -> Seed {
        let mut seed1 = [0u8; 8];
        getrandom::getrandom(&mut seed1).unwrap();

        let hash_vector = <Sha256 as Digest>::digest(&seed1[..]);
        let hash_bytes = hash_vector.as_slice();

        let seed2 = <[u8; 8]>::try_from(&hash_bytes[0..8]).unwrap();
        Seed::from_le_bytes(seed2)
    }
}
