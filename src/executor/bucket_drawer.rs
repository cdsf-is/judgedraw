use std::{cmp::min, collections::HashSet};

use super::{drawer::EventContext, judges};
use crate::model::{bucket::Bucket, buckettype::BucketType, category::Category, judge::Judge};
use anyhow::{anyhow, Result};
use itertools::Itertools;

#[allow(clippy::too_many_arguments)]
pub(crate) fn draw<'j>(
    context: &mut EventContext,
    category: &Category,
    bucket: &'j Bucket,
    report_line: String,
    drawn: &HashSet<&'j Judge>,
    bucket_type_to_equalize: BucketType,
    bucket_is_subset: bool,
    count_to_draw: usize,
) -> Result<HashSet<&'j Judge>> {
    context.reporter.ln(report_line)?;

    let drawn_from_bucket = draw_from_bucket(
        context,
        category,
        bucket,
        drawn,
        count_to_draw,
        bucket_type_to_equalize,
        bucket_is_subset,
    )?;
    judges::dump(
        context.reporter,
        "    ...drawn: ",
        drawn_from_bucket.iter().copied(),
    )?;

    Ok(drawn_from_bucket)
}

fn draw_from_bucket<'j>(
    context: &mut EventContext,
    category: &Category,
    bucket: &'j Bucket,
    drawn: &HashSet<&'j Judge>,
    mut count_to_draw: usize,
    bucket_type_to_equalize: BucketType,
    bucket_is_subset: bool,
) -> Result<HashSet<&'j Judge>> {
    let mut drawn_in_total = drawn.clone();
    let mut drawn_from_bucket = HashSet::<&Judge>::new();

    loop {
        let judges_available_in_bucket =
            load_from_bucket(category, bucket, &drawn_in_total, count_to_draw)?;

        let judges_must_be_seeded = draw_seeded(
            context,
            bucket_type_to_equalize,
            &judges_available_in_bucket,
        )?;

        let drawn_in_iteration = if judges_must_be_seeded.is_empty() {
            draw_randomly(
                context,
                &judges_available_in_bucket,
                count_to_draw,
                bucket_type_to_equalize,
                bucket_is_subset,
            )?
        } else {
            report_forced_seed(context, &judges_must_be_seeded, bucket_type_to_equalize)?;

            let must_be_seeded_count = judges_must_be_seeded.len();
            if must_be_seeded_count > count_to_draw {
                return Err(anyhow!(format!(
                    "Cannot fulfill limit of {} competitions per judge, there is not enough competitions and/or places in them. \
                    Currently, {} judges must be seeded, but only {} places remained available.",
                    context.limit,
                    must_be_seeded_count,
                    count_to_draw
                )));
            }

            Some(judges_must_be_seeded)
        };

        if let Some(drawn_in_iteration) = drawn_in_iteration {
            let drawn_count = drawn_in_iteration.len();
            if drawn_count > 0 {
                drawn_from_bucket.extend(&drawn_in_iteration);

                context
                    .get_equalizer_mut(bucket_type_to_equalize)
                    .advance_use_count(&drawn_in_iteration);

                if drawn_count == count_to_draw {
                    if bucket_is_subset {
                        context
                            .get_equalizer_mut(bucket_type_to_equalize)
                            .reset_temporary_advancing();
                    }

                    return Ok(drawn_from_bucket);
                };

                count_to_draw -= drawn_count;
                drawn_in_total.extend(&drawn_in_iteration);
            }
        }
    }
}

fn load_from_bucket<'j>(
    category: &Category,
    bucket: &'j Bucket,
    drawn_in_total: &HashSet<&Judge>,
    count_to_draw: usize,
) -> Result<Vec<&'j Judge>> {
    let not_seeded_yet = |judge: &Judge| !drawn_in_total.contains(judge);
    let judges_available_in_bucket = bucket.get_judges_to_draw(&not_seeded_yet);

    let judges_available_in_bucket = judges_available_in_bucket
        .iter()
        .filter(|judge| !judge.cannot_judge_with(drawn_in_total))
        .copied()
        .collect::<Vec<&Judge>>();

    let judges_available_in_bucket = judges_available_in_bucket
        .iter()
        .filter(|judge| !judge.cannot_judge_category(category))
        .copied()
        .collect::<Vec<&Judge>>();

    let available_in_bucket = judges_available_in_bucket.len();
    if available_in_bucket < count_to_draw {
        return Err(anyhow!(
            "Not enough judges in the bucket, requested {}, available {}",
            count_to_draw,
            available_in_bucket
        ));
    }

    Ok(judges_available_in_bucket)
}

fn draw_seeded<'j>(
    context: &mut EventContext,
    bucket_type_to_equalize: BucketType,
    judges_available: &[&'j Judge],
) -> Result<HashSet<&'j Judge>> {
    let counts = context.get_equalizer(bucket_type_to_equalize).get_counts();

    let remaining_categories_to_draw = context.to_draw.len() as u64;
    let judges_must_be_seeded: HashSet<&Judge> = judges_available
        .iter()
        .filter(|judge| {
            let remaining_cannot_judge = match judge.cannot_judge() {
                None => 0,
                Some(cannot_judge) => cannot_judge.difference(&context.drawn).count() as u64,
            };

            let already_judging = *counts.get(&judge.label).unwrap();
            remaining_categories_to_draw - remaining_cannot_judge + already_judging <= context.limit
        })
        .copied()
        .collect();

    let mut judges_without_exclusive_pairs = judges_must_be_seeded.clone();

    judges_must_be_seeded.iter().for_each(|judge| {
        let exclusive_with = judge.exclusive_with();
        if let Some(exclusive_with) = exclusive_with {
            let exclusive_pairs_count = judges_without_exclusive_pairs
                .iter()
                .filter(|other_judge| exclusive_with.contains(&other_judge.label))
                .count();
            if exclusive_pairs_count > 0 {
                judges_without_exclusive_pairs.remove(judge);

                if context.verbose {
                    let _ = context.reporter.ln(format!(
                        "    ...judge {} is mutually exclusive with {} other judges, which must also be forcibly seeded due to competitions per judge limit",
                        judge.label, exclusive_pairs_count
                    ));
                }
            }
        }
    });

    if judges_must_be_seeded.len() > judges_without_exclusive_pairs.len() {
        Err(anyhow!(
            "Cannot fulfill limit of {} competitions per judge due to mutually exclusive pairs",
            context.limit
        ))
    } else {
        Ok(judges_without_exclusive_pairs)
    }
}

fn draw_randomly<'j>(
    context: &mut EventContext,
    judges_available_in_bucket: &[&'j Judge],
    count_to_draw: usize,
    bucket_type_to_equalize: BucketType,
    bucket_is_subset: bool,
) -> Result<Option<HashSet<&'j Judge>>> {
    let judges_in_bucket_at_curent_equalization_level = context
        .get_equalizer(bucket_type_to_equalize)
        .filter_by_level(judges_available_in_bucket);

    let available_for_draw = judges_in_bucket_at_curent_equalization_level.len();

    let drawn = if available_for_draw == 0 {
        None
    } else {
        let drawn_from_judges = draw_from_judges(
            context,
            judges_in_bucket_at_curent_equalization_level,
            min(count_to_draw, available_for_draw),
            true,
        );
        if drawn_from_judges.is_empty() {
            None
        } else {
            Some(drawn_from_judges)
        }
    };

    let mut needs_increase_equalization_level = true;
    let drawn = if let Some(drawn) = drawn {
        let drawn_count = drawn.len();
        needs_increase_equalization_level = drawn_count < count_to_draw;
        Some(drawn)
    } else {
        drawn
    };

    if let Some(drawn) = drawn {
        if needs_increase_equalization_level {
            let current_equalization_level =
                increase_equalization_level(context, bucket_type_to_equalize, bucket_is_subset)?;
            if !drawn.is_empty() {
                report_equalization_level(context, &drawn, current_equalization_level)?;
            }
        }

        return Ok(Some(drawn));
    } else {
        increase_equalization_level(context, bucket_type_to_equalize, bucket_is_subset)?;
    }

    Ok(None)
}

pub(crate) fn draw_from_judges<'j>(
    context: &mut EventContext,
    mut judges: Vec<&'j Judge>,
    count_to_draw: usize,
    report_exclusives_rejection: bool,
) -> HashSet<&'j Judge> {
    let mut drawn = HashSet::<&Judge>::new();

    let mut drawn_count = 0;
    let mut available = judges.len();
    loop {
        if drawn_count == count_to_draw {
            break;
        }

        let random = context.generator.get_random_usize(available);
        let judge_drawn = judges.swap_remove(random);
        if judge_drawn.cannot_judge_with(&drawn) {
            if report_exclusives_rejection && context.verbose {
                let _ = context.reporter.ln(format!(
                    "    ...tried to draw {}, but they has to be excluded due to mutual exclusion with somebody already drawn ({})",
                    judge_drawn.label,
                    judges::format(drawn.iter().copied())
                ));
            }
        } else {
            drawn.insert(judge_drawn);
            drawn_count += 1;
        }

        available -= 1;
        if available == 0 {
            break;
        }
    }

    drawn
}

fn increase_equalization_level(
    context: &mut EventContext,
    bucket_type_to_equalize: BucketType,
    bucket_is_subset: bool,
) -> Result<Option<u64>> {
    let equalizer = context.get_equalizer_mut(bucket_type_to_equalize);
    let current_equalization_level = equalizer.get_level();
    if current_equalization_level.is_none() {
        return Ok(None);
    }
    let current_equalization_level = current_equalization_level.unwrap();

    equalizer.advance_level(bucket_is_subset);
    if current_equalization_level > 100 {
        Err(anyhow!(
            "Something went wrong, having 100 levels of equalization is completely unexpected."
        ))
    } else {
        Ok(Some(current_equalization_level))
    }
}

fn report_equalization_level(
    context: &mut EventContext,
    drawn_at_level: &HashSet<&Judge>,
    current_level: Option<u64>,
) -> Result<()> {
    if current_level.is_none() || !context.verbose {
        return Ok(());
    }
    let current_level = current_level.unwrap();

    context.reporter.ln(format!(
        "    ...only {} judging so far {} competitions available, drawing all, continuing with ones with {} competitions",
        judges::format(drawn_at_level.iter().copied()),
        current_level - 1,
        current_level
    ))?;

    Ok(())
}

fn report_forced_seed(
    context: &mut EventContext,
    judges_must_be_seeded: &HashSet<&Judge>,
    bucket_type_to_equalize: BucketType,
) -> Result<()> {
    if !context.verbose {
        return Ok(());
    }

    context.reporter.ln(format!(
        "    ...forcibly seeding {}, it is their last chance to fulfill limit of {} competitions",
        judges::format(judges_must_be_seeded.iter().copied()),
        context.limit,
    ))?;

    for judge in judges_must_be_seeded {
        let all_categories = context
            .to_draw
            .union(&context.drawn)
            .cloned()
            .collect::<HashSet<_>>();

        let can_judge_message = if let Some(cannot_judge) = judge.cannot_judge() {
            let can_judge = all_categories
                .difference(cannot_judge)
                .map(|category| category.to_string())
                .join(", ");
            format!("can judge only {}, ", can_judge)
        } else {
            String::new()
        };

        let equalizer = context.get_equalizer(bucket_type_to_equalize);
        let already_judging = *equalizer.get_counts().get(&judge.label).unwrap();

        context.reporter.ln(format!(
            "       ...judge {}: {} so far judging only {} competition(s)",
            judge.label, can_judge_message, already_judging
        ))?;
    }

    Ok(())
}
