use itertools::Itertools;
use std::collections::{HashMap, HashSet};

use crate::model::judge::Judge;

pub struct Equalizer {
    drawn_count: HashMap<String, u64>,
    equalization_level: Option<u64>,
    temporary_previous_level: Option<u64>, // temporary advancing is used when drawing from subset of global groups (foreign, domestic),
                                           // then only limited judges are available, but increasing their drawing level must not affect
                                           // further drawing from broader (full) set, where still judges with lower count could be present
}

impl Equalizer {
    pub fn new(judges: &[&Judge], equalize: bool) -> Self {
        Equalizer {
            drawn_count: judges
                .iter()
                .map(|&judge| (judge.label.clone(), 0))
                .collect(),
            equalization_level: if equalize { Some(1) } else { None },
            temporary_previous_level: None,
        }
    }

    pub fn filter_by_level<'j>(&self, judges: &[&'j Judge]) -> Vec<&'j Judge> {
        if let Some(equalization_level) = self.equalization_level {
            judges
                .iter()
                .filter(|&judge| {
                    self.drawn_count.get(&judge.label).copied().unwrap_or(0) < equalization_level
                })
                .copied()
                .collect()
        } else {
            judges.to_vec()
        }
    }

    pub fn advance_use_count(&mut self, judges: &HashSet<&Judge>) {
        judges.iter().for_each(|judge| {
            let label = &judge.label;
            let count = self.drawn_count.get(label).copied().unwrap_or(0);
            self.drawn_count.insert(label.to_string(), count + 1);
        });
    }

    pub fn get_level(&self) -> Option<u64> {
        self.equalization_level
    }

    pub fn advance_level(&mut self, temporary: bool) {
        if let Some(level) = self.equalization_level {
            if temporary {
                self.temporary_previous_level = Some(level);
            }
            self.equalization_level = Some(level + 1);
        }
    }

    pub fn reset_temporary_advancing(&mut self) {
        if self.temporary_previous_level.is_some() {
            self.equalization_level = self.temporary_previous_level.take();
        }
    }

    pub fn reset_level(&mut self) {
        if self.equalization_level.is_some() {
            self.equalization_level = Some(1);
        }
    }

    pub fn get_counts(&self) -> &HashMap<String, u64> {
        &self.drawn_count
    }
}

impl ToString for Equalizer {
    fn to_string(&self) -> String {
        self.get_counts()
            .iter()
            .sorted()
            .map(|item| format!("{}({})", item.0, item.1))
            .join(", ")
    }
}
