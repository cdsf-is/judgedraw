use anyhow::Result;
use std::collections::HashSet;

use super::{
    bucket_drawer::{self},
    drawer::{CategoryToDraw, EventContext},
};
use crate::model::{
    buckets::Buckets, buckettype::BucketType, category::Category, discipline::Discipline,
    judge::Judge,
};

pub(crate) fn draw<'j>(
    context: &mut EventContext,
    category_to_draw: &CategoryToDraw,
    buckets: &'j Buckets<'j>,
) -> Result<HashSet<&'j Judge>> {
    context.reporter.ln("---")?;
    context
        .reporter
        .labeledln("  category: ", format!("{}", category_to_draw.category))?;

    context
        .get_equalizer_mut(BucketType::Domestic)
        .reset_level();
    context.get_equalizer_mut(BucketType::Foreign).reset_level();

    let counts_to_draw = calculate_counts_to_draw(category_to_draw);
    let category_draw = draw_from_buckets(context, counts_to_draw, buckets)?;

    Ok(category_draw)
}

struct CountsToDraw {
    category: Category,
    foreign: usize,
    stt_finalist: usize,
    lat_finalist: usize,
    any_domestic: usize,
}

fn calculate_counts_to_draw(category_to_draw: &CategoryToDraw) -> CountsToDraw {
    let specification = category_to_draw.drawing_counts;

    CountsToDraw {
        category: category_to_draw.category.clone(),

        foreign: specification.foreign.unwrap_or(0),

        stt_finalist: match category_to_draw.category.discipline {
            Discipline::Stt => specification.finalist.unwrap_or(0),
            Discipline::Ten => specification.finalist_stt.unwrap_or(0),
            Discipline::Lat | Discipline::ShowStt | Discipline::ShowLat | Discipline::Other => 0,
        },

        lat_finalist: match category_to_draw.category.discipline {
            Discipline::Lat => specification.finalist.unwrap_or(0),
            Discipline::Ten => specification.finalist_lat.unwrap_or(0),
            Discipline::Stt | Discipline::ShowStt | Discipline::ShowLat | Discipline::Other => 0,
        },

        any_domestic: specification.other,
    }
}

fn draw_from_buckets<'j>(
    context: &mut EventContext,
    counts_to_draw: CountsToDraw,
    buckets: &'j Buckets<'j>,
) -> Result<HashSet<&'j Judge>> {
    let mut drawn_in_total = HashSet::new();

    let stt_finalist_to_draw = counts_to_draw.stt_finalist;
    let lat_finalist_to_draw = counts_to_draw.lat_finalist;
    let any_domestic_to_draw = counts_to_draw.any_domestic;

    if counts_to_draw.foreign > 0 {
        let drawn_foreign = bucket_drawer::draw(
            context,
            &counts_to_draw.category,
            buckets.get(&BucketType::Foreign),
            format!(
                "    drawing {} judges from bucket of foreigners...",
                counts_to_draw.foreign
            ),
            &drawn_in_total,
            BucketType::Foreign,
            false,
            counts_to_draw.foreign,
        )?;
        drawn_in_total.extend(drawn_foreign.iter());
    }

    if stt_finalist_to_draw > 0 {
        let drawn_stt = bucket_drawer::draw(
            context,
            &counts_to_draw.category,
            buckets.get(&BucketType::SttFinalist),
            format!(
                "    drawing {} judges from bucket of Stt finalists...",
                stt_finalist_to_draw
            ),
            &drawn_in_total,
            BucketType::Domestic,
            true,
            stt_finalist_to_draw,
        )?;
        drawn_in_total.extend(drawn_stt.iter());
    }

    if lat_finalist_to_draw > 0 {
        let drawn_lat = bucket_drawer::draw(
            context,
            &counts_to_draw.category,
            buckets.get(&BucketType::LatFinalist),
            format!(
                "    drawing {} judges from bucket of Lat finalists...",
                lat_finalist_to_draw
            ),
            &drawn_in_total,
            BucketType::Domestic,
            true,
            lat_finalist_to_draw,
        )?;
        drawn_in_total.extend(drawn_lat.iter());
    }

    if any_domestic_to_draw > 0 {
        let drawn_any = bucket_drawer::draw(
            context,
            &counts_to_draw.category,
            buckets.get(&BucketType::Domestic),
            format!(
                "    drawing {} judges from bucket of unspecified domestic judges...",
                any_domestic_to_draw
            ),
            &drawn_in_total,
            BucketType::Domestic,
            false,
            any_domestic_to_draw,
        )?;
        drawn_in_total.extend(drawn_any.iter());
    }

    Ok(drawn_in_total)
}
