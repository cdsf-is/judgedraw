#![feature(extract_if)]

use anyhow::{anyhow, Result};
use chrono::offset::Local;
use clap::{value_parser, Arg, ArgAction, ArgMatches, Command};
use itertools::Itertools;
use std::{fs, num::NonZeroU32, path::Path};

mod executor;
use executor::{event_drawer, prng::Generator};

mod model;
use model::buckets::Buckets;
use model::drawingspecification::DrawingSpecification;
use model::{discipline::Discipline, judgingtype::JudgingType};

mod io;
use io::{exporter, judgetable, loader, reporter::Reporter};

use self::model::{
    category::{try_category_from_string, Category},
    form::Form,
};

const VERSION: &str = "2.0/SC01_240901";
const DEFAULT_DRAWS: &str = "sc01-240901.json";
const ARG_AJS: &str = "ajs";
const ARG_CATEGORY: &str = "category";
const ARG_DISCIPLINE: &str = "discipline";
const ARG_DRAWS: &str = "draws";
const ARG_EQUALIZE: &str = "equalize";
const ARG_FOREIGNERS: &str = "foreigners";
const ARG_FORM: &str = "form";
const ARG_JUDGES: &str = "judges";
const ARG_LIMIT: &str = "limit";
const ARG_OUTPUT: &str = "output";
const ARG_REPORT: &str = "report";
const ARG_SEED: &str = "seed";
const ARG_SHUFFLE_SEQUENCE: &str = "shuffle";
const ARG_VERBOSE: &str = "verbose";

pub struct Parameters {
    pub judging_type: JudgingType,
    pub categories: Vec<Category>,
    pub foreign: bool,
    pub equalize: bool,
    pub limit: u64,
    pub randomize_categories_order: bool,
}

pub fn main() {
    let arguments = get_arguments();
    let mut reporter = Reporter::new();

    let result = execute(&arguments, &mut reporter);
    if let Err(result) = result {
        let result = reporter.labeledln("\nERROR:\n  ", result.chain().join("\n  "));
        if let Err(message) = result {
            println!("{}", message);
        }
    }

    if let Some(path) = arguments.get_one::<String>(ARG_REPORT) {
        let result = reporter.dump(path);
        if let Err(message) = result {
            println!("{}", message);
        }
    }
}

fn execute(arguments: &ArgMatches, reporter: &mut Reporter) -> Result<()> {
    dump_header(reporter)?;

    let judging_type = if arguments.get_flag(ARG_AJS) {
        JudgingType::AJS30
    } else {
        JudgingType::Skating
    };

    let discipline = arguments.get_one::<Discipline>(ARG_DISCIPLINE).copied();

    let form = arguments.get_one::<Form>(ARG_FORM).copied();

    let categories: Vec<Category> = arguments
        .get_occurrences::<String>(ARG_CATEGORY)
        .unwrap()
        .map(|mut category_number| {
            let Some(category) = category_number.next() else {
                return Err(anyhow!("No category number found."));
            };
            let mut category = try_category_from_string(category.as_str(), form, discipline)?;

            let number = category_number.next();
            if let Some(number) = number {
                match number.parse::<u32>() {
                    Ok(number) => category.competition_number = NonZeroU32::new(number),
                    _ => {
                        return Err(anyhow!("Invalid competition number."));
                    }
                }
            }

            Ok(category)
        })
        .try_collect()?;

    let equalize = arguments.get_flag(ARG_EQUALIZE);

    let foreign = arguments.get_flag(ARG_FOREIGNERS);

    let limit = *arguments.get_one::<u64>(ARG_LIMIT).unwrap();

    let randomize_categories_order = arguments.get_flag(ARG_SHUFFLE_SEQUENCE);

    let verbose = arguments.get_flag(ARG_VERBOSE);

    let parameters = Parameters {
        judging_type,
        categories,
        foreign,
        equalize,
        limit,
        randomize_categories_order,
    };

    dump_range_of_drawing(reporter, &parameters)?;

    let draws_file = arguments.get_one::<String>(ARG_DRAWS).unwrap();
    let drawing_specification = load_draws(draws_file, reporter)?;
    let mut generator = create_generator(arguments, reporter)?;

    let judges_input = arguments.get_one::<String>(ARG_JUDGES).unwrap();
    let judges = loader::load(judges_input)?;
    judgetable::dump(reporter, &judges)?;

    let buckets = Buckets::new(judges.iter().collect());
    if verbose {
        buckets.dump(reporter)?;
    }

    let draw = event_drawer::draw(
        parameters,
        reporter,
        &mut generator,
        &drawing_specification,
        &buckets,
        verbose,
    )?;

    exporter::export(
        &judges,
        &draw,
        &arguments.get_one::<String>(ARG_OUTPUT).unwrap(),
    )?;

    dump_footer(
        reporter,
        generator.get_seed().as_str(),
        draws_file,
        judges_input,
    )?;

    Ok(())
}

fn get_arguments() -> ArgMatches {
    Command::new("CDSF Championship Judge Seed")
        .version(VERSION)
        .about("Utility to draw championship judges according to CDSF competition rules. See 'SC01' supplement of 'Competition rules'.")
        .arg(Arg::new(ARG_AJS)
            .short('a')
            .long("ajs")
            .action(ArgAction::SetTrue)
            .help("Selects that even number of judges will be used."))
        .arg(Arg::new(ARG_CATEGORY)
            .required(true)
            .short('c')
            .long("category")
            .num_args(2)
            .action(ArgAction::Append)
            .help("Single category to draw, repeat for more."))
        .arg(Arg::new(ARG_DISCIPLINE)
            .short('d')
            .long("discipline")
            .num_args(1)
            .value_parser(value_parser!(Discipline))
            .display_order(1)
            .help("Specifies discipline to draw, one value of 'stt', 'lat', 'ten', 'showstt', 'showlat', 'other'."))
        .arg(Arg::new(ARG_DRAWS)
            .long("draws")
            .num_args(1)
            .default_value(DEFAULT_DRAWS)
            .help("JSON configuration file with draws specifications."))
        .arg(Arg::new(ARG_EQUALIZE)
            .short('e')
            .long("equalize")
            .action(ArgAction::SetTrue)
            .help("Switches on count of competitions per judge equalizer."))
        .arg(Arg::new(ARG_FOREIGNERS)
            .short('g')
            .long("foreigners")
            .action(ArgAction::SetTrue)
            .help("Selects that foreign judges will be used."))
        .arg(Arg::new(ARG_FORM)
            .short('f')
            .long("form")
            .num_args(1)
            .value_parser(value_parser!(Form))
            .display_order(2)
            .help("Specifies form to draw, one value of 'couple', 'duo', 'formation', 'group', 'solo', 'team'."))
        .arg(Arg::new(ARG_JUDGES)
            .required(true)
            .short('j')
            .long("judges")
            .num_args(1)
            .display_order(3)
            .help("Specifies input XLSX file (table of judges with their qualifications) to draw from. \
                Recognized columns are 'label', 'name', 'finalist_stt', 'finalist_lat', 'foreign', \
                'exclusive_with', and 'cannot_judge'. 'label' column must be unique, 'exclusive_with' \
                refers to judge labels, 'cannot_judge' refers to categories."))
        .arg(Arg::new(ARG_LIMIT)
            .short('l')
            .long("limit")
            .default_value("2")
            .value_parser(value_parser!(u64))
            .help("Specifies number of competitions that each judge must adjudicate at least."))
        .arg(Arg::new(ARG_OUTPUT)
            .required(true)
            .short('o')
            .long("output")
            .num_args(1)
            .display_order(4)
            .help("Specifies output XLSX file (drawn judges) to save to."))
        .arg(Arg::new("report")
            .short('r')
            .long("report")
            .num_args(1)
            .help("Specifies output TXT file to save report to."))
        .arg(Arg::new(ARG_SEED)
            .short('s')
            .long("seed")
            .num_args(1)
            .help("Seed (16 hex numbers) to generate random numbers, new is created if ommited."))
        .arg(Arg::new(ARG_SHUFFLE_SEQUENCE)
            .short('u')
            .long("shuffle")
            .action(ArgAction::SetTrue)
            .help("Selects that categories will be drawn in random order, not in order of appearance."))
        .arg(Arg::new(ARG_VERBOSE)
            .short('v')
            .long("verbose")
            .action(ArgAction::SetTrue)
            .help("Adds detailed information about drawing."))
        .get_matches()
}

fn dump_header(reporter: &mut Reporter) -> Result<()> {
    reporter.ln("CDSF championship judge drawer")?;
    reporter.labeledln("  ", VERSION)?;
    reporter.ln("  report of drawing")?;

    Ok(())
}

fn load_draws(draws_file: &str, reporter: &mut Reporter) -> Result<DrawingSpecification> {
    reporter.ln("---")?;
    reporter.labeledln("  draws: ", draws_file)?;

    let json = fs::read_to_string(Path::new(draws_file))?;
    DrawingSpecification::from_json(json)
}

fn create_generator(arguments: &ArgMatches, reporter: &mut Reporter) -> Result<Generator> {
    let generator = if let Some(seed) = arguments.get_one::<String>(ARG_SEED) {
        Generator::init_from_seed(seed)
    } else {
        Generator::init_as_random()
    };
    reporter.ln("---")?;
    reporter.labeledln(
        if generator.get_seed_is_new() {
            "  seed (new): "
        } else {
            "  seed (reused): "
        },
        generator.get_seed(),
    )?;

    Ok(generator)
}

fn dump_range_of_drawing(reporter: &mut Reporter, parameters: &Parameters) -> Result<()> {
    reporter.ln("---")?;
    reporter.labeledln("  drawn on: ", Local::now().format("%F %T").to_string())?;
    // TODO reporter.labeledln("  discipline: ", parameters.discipline.to_string())?;
    reporter.labeledln("  categories: ", parameters.categories.iter().join(", "))?;
    reporter.ln(format!(
        "  judging type: {}, using foreigners: {}",
        parameters.judging_type, parameters.foreign
    ))?;
    reporter.ln(format!(
        "  method: equalize counts (-e): {}, force at least {} comps per judge (-l), \
        shuffle categories (-u): {}",
        parameters.equalize, parameters.limit, parameters.randomize_categories_order
    ))?;

    Ok(())
}

fn dump_footer(
    reporter: &mut Reporter,
    seed: &str,
    draws_specification: &str,
    judges_input: &str,
) -> Result<()> {
    reporter.ln("---")?;
    reporter.ln(format!(
        "  completed successfully, \
            store seed '{}', file '{}' and file '{}', if you want to fully \
            reproduce this draw later",
        seed, draws_specification, judges_input
    ))?;

    Ok(())
}
