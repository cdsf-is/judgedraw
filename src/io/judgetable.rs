use anyhow::Result;
use itertools::Itertools;

use crate::{io::reporter::Reporter, model::judge::Judge};

pub fn dump(reporter: &mut Reporter, judges: &[Judge]) -> Result<()> {
    let max_label_length = judges
        .iter()
        .map(|judge| judge.label.len())
        .max()
        .unwrap_or(0);
    let max_name_length = judges
        .iter()
        .map(|judge| judge.name.len())
        .max()
        .unwrap_or(0);

    let row_length = 2 + max_label_length + 3 + max_name_length + 3 + 1 + 3 + 1 + 3 + 1 + 3 + 1 + 2;
    let row_border = format!("    {:–<w$}", "–", w = row_length);
    let border = row_border.as_str();

    let caption_row = format!(
        "    | {:<lw$} | {:<nw$} | {} | {} | {} | {} |",
        "#",
        "N",
        "S",
        "L",
        "T",
        "F",
        lw = max_label_length,
        nw = max_name_length
    );

    reporter.ln("---")?;
    reporter.ln("  judges, as read from input: ")?;
    reporter.ln(border)?;
    reporter.ln(caption_row)?;
    reporter.ln(border)?;

    for judge in judges {
        let judge_row = format!(
            "    | {:<lw$} | {:<nw$} | {} | {} | {} | {} |",
            judge.label,
            judge.name,
            bool_to_x(judge.finalist_stt),
            bool_to_x(judge.finalist_lat),
            bool_to_x(judge.finalist_ten),
            bool_to_x(judge.foreign),
            lw = max_label_length,
            nw = max_name_length
        );
        reporter.ln(judge_row)?;
    }

    reporter.ln(border)?;

    reporter.ln("  judges that cannot judge together: ")?;
    for judge in judges {
        if let Some(exclusive_with) = judge.exclusive_with() {
            reporter.ln(format!(
                "    {}: {}",
                judge.label,
                exclusive_with.iter().join(", ")
            ))?;
        };
    }

    reporter.ln("  judges that cannot judge categories: ")?;
    for judge in judges {
        if let Some(cannot_judge) = judge.cannot_judge() {
            reporter.ln(format!(
                "    {}: {}",
                judge.label,
                cannot_judge.iter().join(", ")
            ))?;
        };
    }

    Ok(())
}

fn bool_to_x(bool: bool) -> char {
    if bool {
        'x'
    } else {
        ' '
    }
}
