use anyhow::{Context, Result};
use simple_excel_writer::{Column, Row, Sheet, SheetWriter, Workbook};

use crate::model::{eventdraw::EventDraw, judge::Judge};

pub fn export(judges: &[Judge], event_draw: &EventDraw, file_name: &str) -> Result<()> {
    let mut workbook = Workbook::create(file_name);
    let mut sheet = workbook.create_sheet("Panel draw");

    define_columns(event_draw, &mut sheet);

    workbook.write_sheet(&mut sheet, |writer| {
        write_header(event_draw, file_name, writer)?;

        for judge in judges {
            write_judge(event_draw, judge, writer)?;
        }

        Ok(())
    })?;

    workbook
        .close()
        .context(format!("Output table with draw: {}", file_name))?;

    Ok(())
}

fn define_columns(event_draw: &EventDraw, sheet: &mut Sheet) {
    sheet.add_column(Column { width: 5.0 });
    sheet.add_column(Column { width: 25.0 });

    let draws = event_draw.iter();
    for _ in draws {
        sheet.add_column(Column { width: 10.0 });
    }
}

fn write_header(
    event_draw: &EventDraw,
    file_name: &str,
    writer: &mut SheetWriter,
) -> std::io::Result<()> {
    let mut row = Row::new();
    row.add_cell("label");
    row.add_empty_cells(1);

    for draw in event_draw.iter() {
        if let Some(competition_number) = draw.0.competition_number {
            row.add_cell(format!("#{competition_number}"));
        } else {
            row.add_empty_cells(1);
        }
    }

    writer.append_row(row)?;

    let mut row = Row::new();
    row.add_empty_cells(1);
    row.add_cell(file_name);
    writer.append_row(row)?;

    let mut row = Row::new();
    row.add_empty_cells(1);
    row.add_cell("name");
    for draw in event_draw.iter() {
        row.add_cell(draw.0.to_string());
    }

    writer.append_row(row)
}

fn write_judge(
    event_draw: &EventDraw,
    judge: &Judge,
    writer: &mut SheetWriter,
) -> std::io::Result<()> {
    let mut row = Row::new();

    row.add_cell(judge.label.as_str());
    row.add_cell(judge.name.as_str());
    for draw in event_draw.iter() {
        if draw.1.contains(judge) {
            row.add_cell("x");
        } else {
            row.add_empty_cells(1);
        }
    }

    writer.append_row(row)
}
