use anyhow::{anyhow, Context, Result};
use calamine::{open_workbook_auto, Data, Range, Reader};
use itertools::Itertools;
use std::{
    collections::{HashMap, HashSet},
    path::{Path, PathBuf},
};

use crate::model::{
    category::{try_category_from_string, Category},
    judge::Judge,
};

type Fields = HashMap<&'static str, usize>;

const FIELD_LABEL: &str = "label";
const FIELD_NAME: &str = "name";
const FIELD_FOREIGN: &str = "foreign";
const FIELD_FINALIST_STT: &str = "finalist_stt";
const FIELD_FINALIST_LAT: &str = "finalist_lat";
const FIELD_FINALIST_TEN: &str = "finalist_ten";
const FIELD_EXCLUSIVE_WITH: &str = "exclusive_with";
const FIELD_CANNOT_JUDGE: &str = "cannot_judge";

pub fn load(path: &str) -> Result<Vec<Judge>> {
    let range = load_xlsx(path)?;
    let judges = load_judges(&range)?;

    Ok(judges)
}

fn check_extension(path: &Path) -> Result<()> {
    let extension = path.extension();
    match extension.and_then(|extension| extension.to_str()) {
        Some("xlsx") | Some("xlsm") | Some("xlsb") | Some("xls") => Ok(()),
        _ => Err(anyhow!(
            "File '{}' is not of known extension (xlsx, xls).",
            path.display()
        )),
    }
}

fn load_xlsx(raw_path: &str) -> Result<Range<Data>> {
    let path = PathBuf::from(raw_path);
    check_extension(&path)?;

    let mut workbook =
        open_workbook_auto(&path).context(format!("Table of judges: {}", raw_path))?;
    let sheet = workbook
        .worksheet_range_at(0)
        .ok_or_else(|| anyhow!("Required sheet no. '{}' has not been found in the file.", 0))??;

    Ok(sheet)
}

fn load_judges(cells: &Range<Data>) -> Result<Vec<Judge>> {
    if cells.rows().count() == 0 {
        return Err(anyhow!("Selected sheet is empty."));
    }
    let fields = create_fields(cells, 0)?;
    load_judges_from_rows(fields, cells, 1)
}

fn create_fields(cells: &Range<Data>, row: usize) -> Result<Fields> {
    let mut fields = Fields::new();

    for column in 0..cells.width() {
        if let Data::String(ref field_candidate) = cells[(row, column)] {
            let field = check_field_name(field_candidate)
                .ok_or_else(|| anyhow!("Field name (column) '{}' is unknown.", field_candidate))?;
            fields.insert(field, column);
        }
    }

    fields
        .get(FIELD_LABEL)
        .ok_or_else(|| anyhow!("Table is missing field name (column) '{}'.", FIELD_LABEL))?;
    fields
        .get(FIELD_NAME)
        .ok_or_else(|| anyhow!("Table is missing field name (column) '{}'.", FIELD_NAME))?;

    Ok(fields)
}

fn check_field_name(field_candidate: &str) -> Option<&'static str> {
    match field_candidate {
        FIELD_LABEL => Some(FIELD_LABEL),
        FIELD_NAME => Some(FIELD_NAME),
        FIELD_FOREIGN => Some(FIELD_FOREIGN),
        FIELD_FINALIST_STT => Some(FIELD_FINALIST_STT),
        FIELD_FINALIST_LAT => Some(FIELD_FINALIST_LAT),
        FIELD_FINALIST_TEN => Some(FIELD_FINALIST_TEN),
        FIELD_EXCLUSIVE_WITH => Some(FIELD_EXCLUSIVE_WITH),
        FIELD_CANNOT_JUDGE => Some(FIELD_CANNOT_JUDGE),
        _ => None,
    }
}

struct References {
    exclusive_with: HashMap<String, String>,
    cannot_judge: HashMap<String, String>,
}

fn load_judges_from_rows(
    fields: Fields,
    cells: &Range<Data>,
    first_row: usize,
) -> Result<Vec<Judge>> {
    let mut judges = Vec::<Judge>::new();
    let mut references = References {
        exclusive_with: HashMap::new(),
        cannot_judge: HashMap::new(),
    };

    for row in first_row..cells.height() {
        let label = fields.get(FIELD_LABEL).map_or(
            Err(anyhow!("FATAL Unexpected: missing FIELD_LABEL")),
            |column| fetch_string(cells, row, *column, true),
        )?;

        let name = fields.get(FIELD_NAME).map_or(
            Err(anyhow!("FATAL Unexpected: missing FIELD_NAME")),
            |column| fetch_string(cells, row, *column, true),
        )?;

        let finalist_stt = fields
            .get(FIELD_FINALIST_STT)
            .map_or(Ok(false), |column| fetch_bool(cells, row, *column, false))?;

        let finalist_lat = fields
            .get(FIELD_FINALIST_LAT)
            .map_or(Ok(false), |column| fetch_bool(cells, row, *column, false))?;

        let finalist_ten = fields
            .get(FIELD_FINALIST_TEN)
            .map_or(Ok(false), |column| fetch_bool(cells, row, *column, false))?;

        let foreign = fields
            .get(FIELD_FOREIGN)
            .map_or(Ok(false), |column| fetch_bool(cells, row, *column, false))?;

        let exclusive_with = fields
            .get(FIELD_EXCLUSIVE_WITH)
            .map_or(Ok(String::default()), |column| {
                fetch_string(cells, row, *column, false)
            })?;
        if !exclusive_with.is_empty() {
            references
                .exclusive_with
                .insert(label.clone(), exclusive_with);
        }

        let cannot_judge = fields
            .get(FIELD_CANNOT_JUDGE)
            .map_or(Ok(String::default()), |column| {
                fetch_string(cells, row, *column, false)
            })?;
        if !cannot_judge.is_empty() {
            references.cannot_judge.insert(label.clone(), cannot_judge);
        }

        let judge = Judge::new(
            label,
            name,
            finalist_stt,
            finalist_lat,
            finalist_ten,
            foreign,
        );
        judges.push(judge);
    }

    unwrap_references(references, &mut judges)?;

    Ok(judges)
}

fn fetch_string(cells: &Range<Data>, row: usize, column: usize, required: bool) -> Result<String> {
    let value = &cells[(row, column)];
    match value {
        Data::Int(ref int) => return Ok(int.to_string()),
        Data::Float(ref float) => return Ok((*float as u64).to_string()),
        Data::String(ref string) => {
            if !string.is_empty() {
                return Ok(string.to_string());
            }
        }
        _ => {}
    }
    if required {
        Err(anyhow!(
            "Cell at row {} in column {} is missing a value (or is not a string neither integer).",
            row + 1,
            (column + 65) as u8 as char
        ))
    } else {
        Ok(String::new())
    }
}

fn fetch_bool(cells: &Range<Data>, row: usize, column: usize, required: bool) -> Result<bool> {
    let value = &cells[(row, column)];
    match value {
        Data::Bool(ref bool) => return Ok(*bool),
        Data::Int(ref int) => return Ok(*int != 0),
        Data::Float(ref float) => return Ok(*float != 0.0),
        Data::String(ref string) => match string.as_str() {
            "1" | "y" | "t" | "x" | "Y" | "T" | "X" => return Ok(true),
            _ => {}
        },
        _ => {}
    }
    if required {
        Err(anyhow!(
            "Cell at row {} in colum {} has not value interpreatable as logic value.",
            row + 1,
            column + 1
        ))
    } else {
        Ok(false)
    }
}

type ExclusivePairs = HashMap<String, HashSet<String>>;

fn unwrap_references(references: References, judges: &mut [Judge]) -> Result<()> {
    let mut exclusive_with_references = HashSet::<String>::new(); // in order to check incorrect references
    let mut exclusive_pairs = ExclusivePairs::new();

    for judge in judges.iter_mut() {
        if let Some(exclusive_with) = references.exclusive_with.get(&judge.label) {
            create_exclusive_pairs(
                &mut exclusive_with_references,
                &mut exclusive_pairs,
                judge,
                exclusive_with.as_str(),
            );
        }

        if let Some(cannot_judge) = references.cannot_judge.get(&judge.label) {
            unwrap_cannot_judge(judge, cannot_judge)
                .context("Error when dereferencing 'cannot_judge'.")?;
        }
    }

    for judge in judges.iter_mut() {
        if let Some(references) = exclusive_pairs.get(&judge.label) {
            judge.set_exclusive_with(references);
        }
        exclusive_with_references.remove(&judge.label);
    }

    if !exclusive_with_references.is_empty() {
        return Err(anyhow!(format!(
            "References {} are wrong, such judges are not known.",
            exclusive_with_references.iter().join(", ")
        )));
    }

    Ok(())
}

fn create_exclusive_pairs(
    exclusive_with_references: &mut HashSet<String>,
    exclusive_pairs: &mut ExclusivePairs,
    judge: &mut Judge,
    exclusive_with: &str,
) {
    if exclusive_with.is_empty() {
        return;
    }

    let labels: HashSet<&str> = exclusive_with.split([' ', ',', ';'].as_ref()).collect();

    let label = &judge.label;
    labels
        .iter()
        .inspect(|&other_label| {
            exclusive_with_references.insert(other_label.to_string());
        })
        .filter_map(|&other_label| {
            if other_label == label {
                None
            } else {
                Some((label.to_string(), other_label.to_string()))
            }
        })
        .for_each(|pair| insert_into_exclusive_pairs(exclusive_pairs, pair));
}

fn insert_into_exclusive_pairs(exclusive_pairs: &mut ExclusivePairs, pair: (String, String)) {
    exclusive_pairs
        .entry(pair.0.clone())
        .or_insert_with(HashSet::new)
        .insert(pair.1.clone());

    exclusive_pairs
        .entry(pair.1)
        .or_insert_with(HashSet::new)
        .insert(pair.0);
}

fn unwrap_cannot_judge(judge: &mut Judge, cannot_judge: &str) -> Result<()> {
    if cannot_judge.is_empty() {
        return Ok(());
    }

    let mut categories = HashSet::<Category>::new();
    let category_names = cannot_judge.split([' ', ',', ';'].as_ref());

    for category_name in category_names.filter(|name| !name.is_empty()) {
        let category = try_category_from_string(category_name, None, None)?;
        categories.insert(category);
    }

    judge.set_cannot_judge(categories);

    Ok(())
}
