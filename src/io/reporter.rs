use std::{fs::File, io::stdout, io::Write, path::Path};

use anyhow::{Context, Result};

pub struct Reporter {
    lines: Vec<String>,
}

impl Reporter {
    pub fn new() -> Self {
        Reporter {
            lines: Vec::<String>::new(),
        }
    }

    pub fn ln<S>(&mut self, line: S) -> Result<()>
    where
        S: Into<String>,
    {
        self.output_and_store(line.into())
    }

    pub fn labeledln<S>(&mut self, label: &str, rest: S) -> Result<()>
    where
        S: Into<String>,
    {
        self.output_and_store(format!("{}{}", label, rest.into()))
    }

    fn output_and_store(&mut self, line: String) -> Result<()> {
        let mut output = line.clone();
        output.push('\n');
        stdout().write_all(output.as_bytes())?;

        self.lines.push(line);

        Ok(())
    }

    pub fn dump(&self, path: &str) -> Result<()> {
        let path = Path::new(path);
        let mut file = File::create(path)?;

        let text = self.lines.join("\n");

        file.write_all(text.as_bytes())
            .with_context(|| format!("Failed to write reports into file {}", path.display()))?;

        Ok(())
    }
}
