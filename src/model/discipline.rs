use std::{fmt::Display, str::FromStr};

use anyhow::anyhow;
use serde::Deserialize;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord, Deserialize)]
pub enum Discipline {
    Stt,
    Lat,
    Ten,
    ShowStt,
    ShowLat,
    Other,
}

impl Display for Discipline {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(format!("{:?}", self).as_str())
    }
}

impl FromStr for Discipline {
    type Err = anyhow::Error;

    fn from_str(candidate: &str) -> Result<Discipline, Self::Err> {
        let lower_case_candidate = candidate.to_lowercase();
        match lower_case_candidate.as_str() {
            "stt" | "s" => Ok(Discipline::Stt),
            "lat" | "l" => Ok(Discipline::Lat),
            "ten" | "t" | "d" => Ok(Discipline::Ten),
            "showstt" | "ss" => Ok(Discipline::ShowStt),
            "showlat" | "sl" => Ok(Discipline::ShowLat),
            "other" | "o" => Ok(Discipline::Other),
            _ => Err(anyhow!(format!("Category {} is unknown.", candidate))),
        }
    }
}
