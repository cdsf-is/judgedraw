use serde_with::DeserializeFromStr;
use std::{fmt::Display, str::FromStr};

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, DeserializeFromStr)]
pub enum JudgingType {
    Skating,
    AJS30,
}

impl Display for JudgingType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(format!("{:?}", self).as_str())
    }
}

const CAT_SKATING_1: &str = "Skating";
const CAT_SKATING_2: &str = "§14.1a)";
const CAT_AJS30_1: &str = "AJS30";
const CAT_AJS30_2: &str = "§14.1b)";

impl FromStr for JudgingType {
    type Err = String;

    fn from_str(candidate: &str) -> Result<JudgingType, Self::Err> {
        match candidate {
            CAT_SKATING_1 | CAT_SKATING_2 => Ok(JudgingType::Skating),
            CAT_AJS30_1 | CAT_AJS30_2 => Ok(JudgingType::AJS30),
            _ => Err("".to_string()),
        }
    }
}
