use anyhow::{anyhow, Context, Result};
use serde::Deserialize;
use serde_json::{self};
use std::collections::HashMap;

use super::{
    age::Age, category::Category, form_discipline::FormDiscipline, judgingtype::JudgingType,
};

#[derive(Deserialize, Copy, Clone)]
pub struct CategoryDrawingCounts {
    pub finalist: Option<usize>,
    pub finalist_stt: Option<usize>,
    pub finalist_lat: Option<usize>,
    pub foreign: Option<usize>,
    pub other: usize,
}

type ReferenceToDraw = String;

type DrawForeignOptionArray = Vec<ReferenceToDraw>;
type JudgingTypeMap = HashMap<JudgingType, DrawForeignOptionArray>;
type AgeMap = HashMap<Age, JudgingTypeMap>;
type CategoryMap = HashMap<FormDiscipline, AgeMap>;

#[derive(Deserialize)]
pub struct DrawingSpecification {
    draws: Option<HashMap<String, CategoryDrawingCounts>>,
    categories: CategoryMap,
}

impl DrawingSpecification {
    pub fn from_json(json: String) -> Result<Self> {
        let draws: DrawingSpecification = serde_json::from_str(json.as_str())
            .with_context(|| "Error parsing drawing specification.")?;

        Ok(draws)
    }

    pub fn get_drawing_counts(
        &self,
        category: &Category,
        judging_type: &JudgingType,
        use_foreign: bool,
    ) -> Result<CategoryDrawingCounts> {
        if self.draws.is_none() {
            return Err(anyhow!("No draws specified."));
        }
        let draws = self.draws.as_ref().unwrap();
        if draws.is_empty() {
            return Err(anyhow!("Draws found, but none is specified."));
        }

        let form_discipline = FormDiscipline::from(category);
        let Some(ages) = self.categories.get(&form_discipline) else {
            return Err(anyhow!(
                "Form.Discipline '{}' is not defined in drawing specification.",
                &form_discipline
            ));
        };
        let judging_types = ages
            .get(&category.age)
            .or_else(|| ages.get(&Age::Other))
            .unwrap();
        let draw_names = judging_types.get(&judging_type).ok_or_else(|| {
            anyhow!(
                "Judging type {} is not defined for {}/{}",
                judging_type,
                category,
                category
            )
        })?;

        if use_foreign && draw_names.len() < 2 {
            return Err(anyhow!(
                "Requested to use foreign judges for {}/{}/{}, but second (foreign) draw is not defined in drawing specification.",
                category, category, judging_type));
        }
        let draw_name = if use_foreign {
            &draw_names[1]
        } else {
            &draw_names[0]
        };
        let draw = draws
            .get(draw_name)
            .ok_or_else(|| anyhow!("Required draw {} has not been found in 'draws'.", draw_name))?;

        if use_foreign && draw.foreign.unwrap_or(0) == 0 {
            return Err(anyhow!(
                "Requested to use foreign judges for {}/{}/{}, but specified draw {} contains none or zero count for foreigners.",
                category, category, judging_type, draw_name));
        }

        Ok(*draw)
    }
}
