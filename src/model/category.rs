use std::{
    fmt::Display,
    hash::{Hash, Hasher},
    num::NonZeroU32,
    str::FromStr,
};

use anyhow::anyhow;

use super::{age::Age, discipline::Discipline, form::Form};

#[derive(Clone, Eq, Ord)]
pub struct Category {
    pub age: Age,
    pub discipline: Discipline,
    pub form: Form,
    pub competition_number: Option<NonZeroU32>,
}

impl Display for Category {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(format!("{}.{}.{}", self.form, self.discipline, self.age).as_str())
    }
}

impl PartialEq for Category {
    fn eq(&self, other: &Self) -> bool {
        self.age == other.age && self.discipline == other.discipline && self.form == other.form
    }
}

impl PartialOrd for Category {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(
            self.age
                .cmp(&other.age)
                .then(self.discipline.cmp(&other.discipline))
                .then(self.form.cmp(&other.form)),
        )
    }
}

impl Hash for Category {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.age.hash(state);
        self.discipline.hash(state);
        self.form.hash(state);
    }
}

pub fn try_category_from_string(
    value: &str,
    form: Option<Form>,
    discipline: Option<Discipline>,
) -> Result<Category, anyhow::Error> {
    let parts: Vec<&str> = value.split('.').collect();
    match (parts.len(), form, discipline) {
        (0, _, _) => Err(anyhow!(
            "Invalid 'form.discipline.age' format, input is empty."
        )),
        (1, Some(form), Some(discipline)) => Ok(Category {
            form,
            discipline,
            age: Age::from_str(parts[0])?,
            competition_number: None,
        }),
        (2, None, Some(discipline)) => Ok(Category {
            form: Form::from_str(parts[0])?,
            discipline,
            age: Age::from_str(parts[1])?,
            competition_number: None,
        }),
        (2, Some(form), None) => Ok(Category {
            form,
            discipline: Discipline::from_str(parts[0])?,
            age: Age::from_str(parts[1])?,
            competition_number: None,
        }),
        (3, _, _) => Ok(Category {
            form: Form::from_str(parts[0])?,
            discipline: Discipline::from_str(parts[1])?,
            age: Age::from_str(parts[2])?,
            competition_number: None,
        }),
        _ => Err(anyhow!(
            "Invalid 'form.discipline.age' format, expected 1, 2 or 3 parts, got '{value}'.",
        )),
    }
}
