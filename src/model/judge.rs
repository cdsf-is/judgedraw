use std::{
    collections::HashSet,
    hash::{Hash, Hasher},
};

use super::category::Category;

#[derive(Eq)]
pub struct Judge {
    pub label: String,
    pub name: String,
    pub finalist_stt: bool,
    pub finalist_lat: bool,
    pub finalist_ten: bool,
    pub foreign: bool,
    exclusive_with: Option<HashSet<String>>,
    cannot_judge: Option<HashSet<Category>>,
}

impl PartialEq for Judge {
    fn eq(&self, other: &Self) -> bool {
        self.label == other.label
    }
}

impl Hash for Judge {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.label.hash(state);
    }
}

impl Judge {
    pub fn new(
        label: String,
        name: String,
        finalist_stt: bool,
        finalist_lat: bool,
        finalist_ten: bool,
        foreign: bool,
    ) -> Self {
        Judge {
            label,
            name,
            finalist_stt,
            finalist_lat,
            finalist_ten,
            foreign,
            exclusive_with: None,
            cannot_judge: None,
        }
    }

    pub fn exclusive_with(&self) -> Option<&HashSet<String>> {
        self.exclusive_with.as_ref()
    }

    pub fn set_exclusive_with(&mut self, exclusive_with: &HashSet<String>) {
        self.exclusive_with = Some(exclusive_with.iter().cloned().collect());
    }

    pub fn cannot_judge(&self) -> Option<&HashSet<Category>> {
        self.cannot_judge.as_ref()
    }

    pub fn set_cannot_judge(&mut self, categories: HashSet<Category>) {
        self.cannot_judge = Some(categories);
    }

    pub fn cannot_judge_with(&self, judges_to_check: &HashSet<&Judge>) -> bool {
        self.exclusive_with
            .as_ref()
            .map(|exclusive_with| {
                judges_to_check
                    .iter()
                    .any(|judge_to_check| exclusive_with.contains(&judge_to_check.label))
            })
            .unwrap_or_default()
    }

    pub fn cannot_judge_category(&self, category: &Category) -> bool {
        self.cannot_judge
            .as_ref()
            .map(|cannot_judge| cannot_judge.contains(category))
            .unwrap_or_default()
    }
}
