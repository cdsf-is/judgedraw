use std::{fmt::Display, str::FromStr};

use anyhow::anyhow;
use serde::Deserialize;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord, Deserialize)]
pub enum Form {
    Couple,
    Duo,
    Formation,
    Group,
    Solo,
    Team,
}

impl Display for Form {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(format!("{:?}", self).as_str())
    }
}

impl FromStr for Form {
    type Err = anyhow::Error;

    fn from_str(candidate: &str) -> Result<Form, Self::Err> {
        let lower_case_candidate = candidate.to_lowercase();
        match lower_case_candidate.as_str() {
            "couple" | "c" => Ok(Form::Couple),
            "duo" | "d" => Ok(Form::Duo),
            "formation" | "f" => Ok(Form::Formation),
            "group" | "g" => Ok(Form::Group),
            "solo" | "s" => Ok(Form::Solo),
            "team" | "t" => Ok(Form::Team),
            _ => Err(anyhow!(format!("Category {} is unknown.", candidate))),
        }
    }
}
