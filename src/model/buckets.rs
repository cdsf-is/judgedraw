use anyhow::Result;
use std::collections::HashMap;

use crate::{
    executor::judges,
    io::reporter::Reporter,
    model::{bucket::Bucket, buckettype::BucketType, judge::Judge},
};

type BucketMap<'j> = HashMap<BucketType, Bucket<'j>>;

pub struct Buckets<'j> {
    type_map: BucketMap<'j>,
}

impl<'j> Buckets<'j> {
    pub fn new(judges: Vec<&'j Judge>) -> Self {
        Buckets {
            type_map: Buckets::fill(&judges),
        }
    }

    fn fill(judges: &[&'j Judge]) -> BucketMap<'j> {
        let mut all = Bucket::new();
        let mut stt_finalist = Bucket::new();
        let mut lat_finalist = Bucket::new();
        let mut ten_finalist = Bucket::new();
        let mut domestic = Bucket::new();
        let mut foreign = Bucket::new();

        for judge in judges {
            all.insert(judge);

            if judge.finalist_stt {
                stt_finalist.insert(judge);
            }

            if judge.finalist_lat {
                lat_finalist.insert(judge);
            }

            if judge.finalist_ten {
                ten_finalist.insert(judge);
            }

            if judge.foreign {
                foreign.insert(judge);
            } else {
                domestic.insert(judge);
            }
        }

        let mut buckets = BucketMap::new();

        buckets.insert(BucketType::All, all);
        buckets.insert(BucketType::SttFinalist, stt_finalist);
        buckets.insert(BucketType::LatFinalist, lat_finalist);
        buckets.insert(BucketType::TenFinalist, ten_finalist);
        buckets.insert(BucketType::Domestic, domestic);
        buckets.insert(BucketType::Foreign, foreign);

        buckets
    }

    pub fn get(&self, bucket_type: &BucketType) -> &Bucket {
        self.type_map.get(bucket_type).unwrap()
    }

    pub fn dump(&self, reporter: &mut Reporter) -> Result<()> {
        reporter.ln("---")?;
        reporter.ln("  buckets:")?;

        self.dump_bucket(reporter, "    stt fin: ", BucketType::SttFinalist)?;
        self.dump_bucket(reporter, "    lat fin: ", BucketType::LatFinalist)?;
        self.dump_bucket(reporter, "    domestic: ", BucketType::Domestic)?;
        self.dump_bucket(reporter, "    foreign: ", BucketType::Foreign)?;

        Ok(())
    }

    fn dump_bucket(
        &self,
        reporter: &mut Reporter,
        name: &str,
        bucket_type: BucketType,
    ) -> Result<()> {
        let bucket = self.get(&bucket_type);
        judges::dump(reporter, name, bucket.get_judges().into_iter())
    }
}
