use std::{fmt::Display, str::FromStr};

use anyhow::{anyhow, Error};
use serde::Deserialize;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Deserialize, PartialOrd, Ord)]
pub enum Age {
    Juveniles,
    JuvenilesII,
    Junior,
    JuniorI,
    JuniorII,
    Youth,
    Under21,
    Adult,
    Professional,
    Senior,
    SeniorI,
    SeniorII,
    SeniorIII,
    SeniorIV,
    Other,
}

impl Display for Age {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(format!("{:?}", self).as_str())
    }
}

impl FromStr for Age {
    type Err = Error;

    fn from_str(candidate: &str) -> Result<Age, Self::Err> {
        let lower_case_candidate = candidate.to_lowercase();
        match lower_case_candidate.as_str() {
            "juveniles" | "de" | "c" => Ok(Age::Juveniles),
            "juvenilesii" | "dii" | "d2" | "cii" | "c2" => Ok(Age::JuvenilesII),
            "junior" | "j" => Ok(Age::Junior),
            "juniori" | "ji" | "j1" => Ok(Age::JuniorI),
            "juniorii" | "jii" | "j2" => Ok(Age::JuniorII),
            "youth" | "y" => Ok(Age::Youth),
            "under21" | "u21" => Ok(Age::Under21),
            "adult" | "do" | "a" => Ok(Age::Adult),
            "profi" | "p" => Ok(Age::Professional),
            "senior" | "s" => Ok(Age::Senior),
            "seniori" | "si" | "s1" => Ok(Age::SeniorI),
            "seniorii" | "sii" | "s2" => Ok(Age::SeniorII),
            "senioriii" | "siii" | "s3" => Ok(Age::SeniorIII),
            "senioriv" | "siv" | "s4" => Ok(Age::SeniorIV),
            _ => Err(anyhow!(format!("Category {} is unknown.", candidate))),
        }
    }
}
