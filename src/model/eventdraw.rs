use super::{category::Category, judge::Judge};
use std::collections::{btree_map::Iter, BTreeMap, HashSet};

pub type CategoryDraw<'j> = HashSet<&'j Judge>;

pub struct EventDraw<'j> {
    event_draw: BTreeMap<Category, CategoryDraw<'j>>,
}

impl<'j> EventDraw<'j> {
    pub fn new() -> Self {
        EventDraw {
            event_draw: BTreeMap::new(),
        }
    }

    pub fn set_category(&mut self, category: Category, draw: CategoryDraw<'j>) {
        self.event_draw.insert(category, draw);
    }

    pub fn iter(&self) -> Iter<Category, CategoryDraw> {
        self.event_draw.iter()
    }
}
