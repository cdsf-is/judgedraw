use std::{fmt::Display, str::FromStr};

use anyhow::anyhow;
use serde::Deserialize;

use super::{category::Category, discipline::Discipline, form::Form};

#[derive(Clone, Copy, Deserialize, PartialEq, Eq, Hash)]
#[serde(try_from = "String")]
pub struct FormDiscipline {
    pub form: Form,
    pub discipline: Discipline,
}

impl Display for FormDiscipline {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(format!("{}.{}", self.form, self.discipline).as_str())
    }
}

impl TryFrom<String> for FormDiscipline {
    type Error = anyhow::Error;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let parts: Vec<&str> = value.split('.').collect();
        if parts.len() != 2 {
            Err(anyhow!("Invalid 'form.discipline' format."))?;
        } else if parts[0].is_empty() {
            Err(anyhow!("Missing form of the 'form.discipline'."))?;
        } else if parts[1].is_empty() {
            Err(anyhow!("Missing discipline of the 'form.discipline'."))?;
        }
        let form = Form::from_str(parts[0])?;
        let discipline = Discipline::from_str(parts[1])?;
        Ok(Self { form, discipline })
    }
}

impl From<&Category> for FormDiscipline {
    fn from(value: &Category) -> Self {
        Self {
            form: value.form,
            discipline: value.discipline,
        }
    }
}
