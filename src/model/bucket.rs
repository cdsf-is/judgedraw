use std::collections::BTreeMap;

use crate::model::judge::Judge;

type Judges<'j> = BTreeMap<String, &'j Judge>;
// BTree here, as we must sort judges somehow in order to get repeatable results
// when seed is reused (-s argument)

pub struct Bucket<'j> {
    judges: Judges<'j>,
}

impl<'j> Bucket<'j> {
    pub fn new() -> Self {
        Bucket {
            judges: Judges::new(),
        }
    }

    pub fn insert(&mut self, judge: &'j Judge) {
        self.judges.insert(judge.label.clone(), judge);
    }

    pub fn get_judges(&self) -> Vec<&Judge> {
        self.judges.values().copied().collect()
    }

    pub fn get_judges_to_draw<F>(&self, filter: F) -> Vec<&Judge>
    where
        F: Fn(&Judge) -> bool,
    {
        self.judges
            .values()
            .filter(|&&judge| filter(judge))
            .copied()
            .collect()
    }
}
